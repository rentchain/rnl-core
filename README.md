RentalChain Core integration/staging repository
=====================================

RentalChain will unleash the power of blockchain technology to build a platform that will change the Rent&Lease Industry forever.

RentalChain is a unique Peer-to-Peer rental and lease project that uses blockchain as the main technology. 
Having this framework, the Ecosystem will serve as a decentralized app, eliminating intermediaries like agencies or promoters bringing in direct contact the owners with the tenants. 

More information at [RentalChain.net](https://www.RentalChain.net)



### RNL Coin Specs

|   |   |
|---|---|
|Type|POS + Masternode|
|Reward/Block|50 RNL|
|Reward/Block Masternode|40 RNL (80%)|
|Reward/Block Stake|10 RNL (20%)|
|Block Time|240 Seconds|
|Maturity|100 Blocks|
|Difficulty Retargeting|40 Blocks|
|Masternode Collateral|99999 RNL|
|Masternode Collateral Locked|YES|
|Masternode Collateral Time Locked|6 Months|
|P2P port|19101|
|RPC port|19102|
|Max Coin Supply|600,000,000 RNL|


License
-------

Rental Chain Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

For more info about [RentalChain](https://www.RentalChain.net) follow us on our social networks.
