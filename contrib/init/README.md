Sample configuration files for:

SystemD: rentalchaind.service
Upstart: rentalchaind.conf
OpenRC:  rentalchaind.openrc
         rentalchaind.openrcconf
CentOS:  rentalchaind.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
