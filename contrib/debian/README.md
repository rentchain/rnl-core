
Debian
====================
This directory contains files used to package rentalchaind/rentalchain-qt
for Debian-based Linux systems. If you compile rentalchaind/rentalchain-qt yourself, there are some useful files here.

## rentalchain: URI support ##


rentalchain-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install rentalchain-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your rentalchainqt binary to `/usr/bin`
and the `../../share/pixmaps/rentalchain128.png` to `/usr/share/pixmaps`

rentalchain-qt.protocol (KDE)

