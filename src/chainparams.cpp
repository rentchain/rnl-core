// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2014 The Bitcoin developers
// Copyright (c) 2014-2015 The Dash developers
// Copyright (c) 2015-2018 The PIVX developers
// Copyright (c) 2018-2020 The RentalChain Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "chainparams.h"

#include "random.h"
#include "util.h"
#include "utilstrencodings.h"

#include <assert.h>

#include <boost/assign/list_of.hpp>

using namespace std;
using namespace boost::assign;

struct SeedSpec6 {
    uint8_t addr[16];
    uint16_t port;
};

#include "chainparamsseeds.h"

/**
 * Main network
 */


static void convertSeed6(std::vector<CAddress>& vSeedsOut, const SeedSpec6* data, unsigned int count)
{
    const int64_t nOneWeek = 7 * 24 * 60 * 60;
    for (unsigned int i = 0; i < count; i++) {
        struct in6_addr ip;
        memcpy(&ip, data[i].addr, sizeof(ip));
        CAddress addr(CService(ip, data[i].port));
        addr.nTime = GetTime() - GetRand(nOneWeek) - nOneWeek;
        vSeedsOut.push_back(addr);
    }
}

static Checkpoints::MapCheckpoints mapCheckpoints =
    boost::assign::map_list_of
    (0, uint256("0000080e5fa95746a084a31a68ce99aee8378cf28f1cf324f05468ed56715ce1"));
static const Checkpoints::CCheckpointData data = {
    &mapCheckpoints,
    1606590000,   // * UNIX timestamp of last checkpoint block
    0,            // * total number of transactions between genesis and last checkpoint (the tx=... number in the SetBestChain debug.log lines)
    2000          // * estimated number of transactions per day after checkpoint
};

static Checkpoints::MapCheckpoints mapCheckpointsTestnet =
    boost::assign::map_list_of
    (0, uint256("00000b38c794d6af9537bf16823c3bf3387306201167e0c28e552982725d7b10"));
static const Checkpoints::CCheckpointData dataTestnet = {
    &mapCheckpointsTestnet,
    1606590000,
    0,
    5};

static Checkpoints::MapCheckpoints mapCheckpointsRegtest =
    boost::assign::map_list_of(0, uint256("0x001"));
static const Checkpoints::CCheckpointData dataRegtest = {
    &mapCheckpointsRegtest,
    1606590000,
    0,
    100};

class CMainParams : public CChainParams
{
public:
    CMainParams()
    {
        networkID = CBaseChainParams::MAIN;
        strNetworkID = "main";
        pchMessageStart[0] = 0x99;
        pchMessageStart[1] = 0x3c;
        pchMessageStart[2] = 0xff;
        pchMessageStart[3] = 0x1c;
        vAlertPubKey = ParseHex("04874664abe7588dc5fddc478afc8b45600d8fdd2cdbc2f4c56c33c55ca9df776fb44f6afc1b96b711f077095fe8e819b72badb562ac5a15a6d4e1e2e71fab0cfa");
        nDefaultPort = 19101;
        bnProofOfWorkLimit = ~uint256(0) >> 20; // RentalChain starting difficulty is 1 / 2^12
        nSubsidyHalvingInterval = 525600; // one year
        nMaxReorganizationDepth = 100;
        nEnforceBlockUpgradeMajority = 750;
        nRejectBlockOutdatedMajority = 950;
        nToCheckBlockUpgradeMajority = 1000;
        nMinerThreads = 0;
        nTargetTimespan = 4 * 60; // RentalChain: every block
        nTargetSpacing = 4 * 60;  // RentalChain: 4 minutes
        nLastPOWBlock = 360;
        nMaturity = 100;
        nCollateralMaturity = 262800;
        nCollateralMaturityEnforcementHeight = 3600;
        nCollateralMaturityTimeWindow = 15 * 24 * 3;
        nMasternodeCountDrift = 20;
        nModifierUpdateBlock = 1;
        nMaxMoneyOut = 92000000000 * COIN;
        nMasternodeCollateral = 99999; 
        nStakeMinConfirmations = 300;   // Required number of confirmations
        nStakeMinAmount = 20000 * COIN;   // Minimum required staking amount

        const char* pszTimestamp = "Renting can be an example of the sharing economy";
        CMutableTransaction txNew;
        txNew.vin.resize(1);
        txNew.vout.resize(1);
        txNew.vin[0].scriptSig = CScript() << 486604799 << CScriptNum(4) << vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
        txNew.vout[0].nValue = 50 * COIN;
        txNew.vout[0].scriptPubKey = CScript() << ParseHex("04e4c04550ce79ac8f90a95001212da4ba2e09901a5f55c1ad96cd0b03d4f51d274fae2fb57822dde85f6c3bbcd24ebe6edf04da1ef53c67f15339406a73436c1c") << OP_CHECKSIG;
        genesis.vtx.push_back(txNew);
        genesis.hashPrevBlock = 0;
        genesis.hashMerkleRoot = genesis.BuildMerkleTree();
        genesis.nVersion = 1;
        genesis.nTime = 1606590000;
        genesis.nBits = 0x1e0ffff0;
        genesis.nNonce = 7096902;

        hashGenesisBlock = genesis.GetHash();
        assert(hashGenesisBlock == uint256("0x0000080e5fa95746a084a31a68ce99aee8378cf28f1cf324f05468ed56715ce1"));
        assert(genesis.hashMerkleRoot == uint256("0xba528b173ce03b02c56811feb18a961aaff5a52ea9efd58d75c25c0be4cc83be"));

        vSeeds.push_back(CDNSSeedData("dseed1", "dseed1.rentalchain.net"));
        vSeeds.push_back(CDNSSeedData("dseed2", "dseed2.rentalchain.net"));
		
        
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1, 60); // Rentalchain addresses start with 'R'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1, 18); // Rentalchain script addresses start with '8'
        base58Prefixes[SECRET_KEY] = std::vector<unsigned char>(1, 137);    // Rentalchain private keys start with 'x'
        // RentalChain BIP32 pubkeys start with 'xpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
        // RentalChain BIP32 pubkeys start with 'xprv' (Bitcoin defaults)		
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();
        // 	BIP44 coin type is '591' from https://github.com/satoshilabs/slips/blob/master/slip-0044.md
        nExtCoinType = 591;

        convertSeed6(vFixedSeeds, pnSeed6_main, ARRAYLEN(pnSeed6_main));

        fRequireRPCPassword = true;
        fMiningRequiresPeers = true;
        fAllowMinDifficultyBlocks = false;
        fDefaultConsistencyChecks = false;
        fRequireStandard = true;
        fMineBlocksOnDemand = false;
        fSkipProofOfWorkCheck = false;
        fTestnetToBeDeprecatedFieldRPC = false;
        fHeadersFirstSyncingActive = false;

        nPoolMaxTransactions = 3;
        strSporkKey = "043c53e06160345c663b164376a8051b7d86a2b8b111675eaed4aa204258041c58651d6ca3d58dd57e1b5e0e38f069b759ef4e989df630130685d00a7b1014b437";
        strPrivatesendPoolDummyAddress = "RBBkNhNrUFoamd3sZGJoLsJ2WG4qNqQ4d1";
        nStartMasternodePayments = 1606590000;
    }

    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return data;
    }
};
static CMainParams mainParams;

/**
 * Testnet (v3)
 */
class CTestNetParams : public CMainParams
{
public:
    CTestNetParams()
    {
        networkID = CBaseChainParams::TESTNET;
        strNetworkID = "test";
        pchMessageStart[0] = 0xd4;
        pchMessageStart[1] = 0x1b;
        pchMessageStart[2] = 0x73;
        pchMessageStart[3] = 0xee;
        vAlertPubKey = ParseHex("04529535f373e2523888e5b753a936e4e08115ec80715cf566f23ebf8ebb786d6599f58de0e5b0d55f683346422998e3ac6439cc5d985f87fa497455940b8779cd");
        nDefaultPort = 19103;
        nEnforceBlockUpgradeMajority = 51;
        nRejectBlockOutdatedMajority = 75;
        nToCheckBlockUpgradeMajority = 100;
        nMinerThreads = 0;
        nTargetTimespan = 4 * 60; // RentalChain: every block
        nTargetSpacing = 4 * 60;  // RentalChain: 4 minutes
        nLastPOWBlock = 360;
        nMaturity = 100;
        nCollateralMaturity = 360;
        nCollateralMaturityEnforcementHeight = 1000;
        nCollateralMaturityTimeWindow = 24 * 15;
        nMasternodeCountDrift = 4;
        nModifierUpdateBlock = 1; 
        nMaxMoneyOut = 92000000000 * COIN;
        nMasternodeCollateral = 99999; 
        nStakeMinConfirmations = 100;   // Required number of confirmations
        nStakeMinAmount = 100 * COIN;    // Minimum required staking amount

        //! Modify the testnet genesis block so the timestamp is valid for a later start.
        genesis.nTime = 1606590000;
        genesis.nNonce = 3719525;

        hashGenesisBlock = genesis.GetHash();
        assert(hashGenesisBlock == uint256("0x00000b38c794d6af9537bf16823c3bf3387306201167e0c28e552982725d7b10"));

        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("dseed1", "dseed1.rentalchain.net"));
        vSeeds.push_back(CDNSSeedData("dseed2", "dseed2.rentalchain.net"));

        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1, 127); // Testnet rentalchain addresses start with 't'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1, 20);  // Testnet rentalchain script addresses start with '9'
        base58Prefixes[SECRET_KEY] = std::vector<unsigned char>(1, 239);     // Testnet private keys start with '9' or 'c' (Bitcoin defaults)
		// RentalChain BIP32 pubkeys start with 'xpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
		// RentalChain BIP32 pubkeys start with 'xprv' (Bitcoin defaults)		
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();
        // Testnet rentalchain BIP44 coin type is '1' (All coin's testnet default)
        nExtCoinType = 1;

        convertSeed6(vFixedSeeds, pnSeed6_test, ARRAYLEN(pnSeed6_test));

        fRequireRPCPassword = true;
        fMiningRequiresPeers = true;
        fAllowMinDifficultyBlocks = true;
        fDefaultConsistencyChecks = false;
        fRequireStandard = false;
        fMineBlocksOnDemand = false;
        fTestnetToBeDeprecatedFieldRPC = true;

        nPoolMaxTransactions = 2;
        strSporkKey = "046fd0edc845e9b93a49a19951ea76ab7e4467120684a09aaf600eb6239a4537783caa9f81addcb2f24bae8972b093e381f40be26b1febf95d28eb9e764bd8f44f";
        strPrivatesendPoolDummyAddress = "RUUfHKAv71bUdeG9ZELKZi9hjbdSazS4t4";
        nStartMasternodePayments = 1606590000;
    }
    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return dataTestnet;
    }
};
static CTestNetParams testNetParams;

/**
 * Regression test
 */
class CRegTestParams : public CTestNetParams
{
public:
    CRegTestParams()
    {
        networkID = CBaseChainParams::REGTEST;
        strNetworkID = "regtest";
        strNetworkID = "regtest";
        pchMessageStart[0] = 0x5d;
        pchMessageStart[1] = 0xb3;
        pchMessageStart[2] = 0x21;
        pchMessageStart[3] = 0x4b;
        nSubsidyHalvingInterval = 150;
        nEnforceBlockUpgradeMajority = 750;
        nRejectBlockOutdatedMajority = 950;
        nToCheckBlockUpgradeMajority = 1000;
        nMinerThreads = 1;
        nTargetTimespan = 24 * 60 * 60; // RentalChain: 1 day
        nTargetSpacing = 1 * 60;        // RentalChain: 1 minutes
        bnProofOfWorkLimit = ~uint256(0) >> 1;
        genesis.nTime = 1606590000;
        genesis.nBits = 0x207fffff;
        genesis.nNonce = 1;

        hashGenesisBlock = genesis.GetHash();
        nDefaultPort = 19105;
        assert(hashGenesisBlock == uint256("0x678b2377432476d64e7093fe6b1068fececa3ccff41f06b07b8d2432896c5296"));

        vFixedSeeds.clear(); //! Testnet mode doesn't have any fixed seeds.
        vSeeds.clear();      //! Testnet mode doesn't have any DNS seeds.

        fRequireRPCPassword = false;
        fMiningRequiresPeers = false;
        fAllowMinDifficultyBlocks = true;
        fDefaultConsistencyChecks = true;
        fRequireStandard = false;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;
    }
    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        return dataRegtest;
    }
};
static CRegTestParams regTestParams;

/**
 * Unit test
 */
class CUnitTestParams : public CMainParams, public CModifiableParams
{
public:
    CUnitTestParams()
    {
        networkID = CBaseChainParams::UNITTEST;
        strNetworkID = "unittest";
        nDefaultPort = 47008;
        vFixedSeeds.clear(); //! Unit test mode doesn't have any fixed seeds.
        vSeeds.clear();      //! Unit test mode doesn't have any DNS seeds.

        fRequireRPCPassword = false;
        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = true;
        fAllowMinDifficultyBlocks = false;
        fMineBlocksOnDemand = true;
    }

    const Checkpoints::CCheckpointData& Checkpoints() const
    {
        // UnitTest share the same checkpoints as MAIN
        return data;
    }

    //! Published setters to allow changing values in unit test cases
    virtual void setSubsidyHalvingInterval(int anSubsidyHalvingInterval) { nSubsidyHalvingInterval = anSubsidyHalvingInterval; }
    virtual void setEnforceBlockUpgradeMajority(int anEnforceBlockUpgradeMajority) { nEnforceBlockUpgradeMajority = anEnforceBlockUpgradeMajority; }
    virtual void setRejectBlockOutdatedMajority(int anRejectBlockOutdatedMajority) { nRejectBlockOutdatedMajority = anRejectBlockOutdatedMajority; }
    virtual void setToCheckBlockUpgradeMajority(int anToCheckBlockUpgradeMajority) { nToCheckBlockUpgradeMajority = anToCheckBlockUpgradeMajority; }
    virtual void setDefaultConsistencyChecks(bool afDefaultConsistencyChecks) { fDefaultConsistencyChecks = afDefaultConsistencyChecks; }
    virtual void setAllowMinDifficultyBlocks(bool afAllowMinDifficultyBlocks) { fAllowMinDifficultyBlocks = afAllowMinDifficultyBlocks; }
    virtual void setSkipProofOfWorkCheck(bool afSkipProofOfWorkCheck) { fSkipProofOfWorkCheck = afSkipProofOfWorkCheck; }
};
static CUnitTestParams unitTestParams;


static CChainParams* pCurrentParams = 0;

CModifiableParams* ModifiableParams()
{
    assert(pCurrentParams);
    assert(pCurrentParams == &unitTestParams);
    return (CModifiableParams*)&unitTestParams;
}

const CChainParams& Params()
{
    assert(pCurrentParams);
    return *pCurrentParams;
}

CChainParams& Params(CBaseChainParams::Network network)
{
    switch (network) {
    case CBaseChainParams::MAIN:
        return mainParams;
    case CBaseChainParams::TESTNET:
        return testNetParams;
    case CBaseChainParams::REGTEST:
        return regTestParams;
    case CBaseChainParams::UNITTEST:
        return unitTestParams;
    default:
        assert(false && "Unimplemented network");
        return mainParams;
    }
}

void SelectParams(CBaseChainParams::Network network)
{
    SelectBaseParams(network);
    pCurrentParams = &Params(network);
}

bool SelectParamsFromCommandLine()
{
    CBaseChainParams::Network network = NetworkIdFromCommandLine();
    if (network == CBaseChainParams::MAX_NETWORK_TYPES)
        return false;

    SelectParams(network);
    return true;
}

bool CChainParams::nonCollateralMaturity(int nTxHeight) const {

   int nDiv = nTxHeight / nCollateralMaturity;
   int nRem = nTxHeight % nCollateralMaturity;

   if(nDiv >= 1 && nRem <= nCollateralMaturityTimeWindow)
       return false;
   return true;
}
